%global orig_src_dir %{getenv:SRC_DIR}
%global rust_target_arch %{getenv:CARGO_BUILD_TARGET}
%global tgt_src_dir %{getenv:CARGO_TARGET_DIR}
%global tgt_profile %{getenv:RUST_PROFILE}
Name: semver-formatter
Version:        %{getenv:RPM_VERSION}
Release:        %{?getenv:RPM_RELEASE}%{?dist}
Summary:        A simple binary to format a given semver version into RPM specific versions.
License:        GPLv3
URL:            https://gitlab.com/ds_2/semver-formatter/
%description
A simple binary to format a given semver version into RPM specific versions.

%install
rm -rf $RPM_BUILD_ROOT
install -m 0755 -D %{tgt_src_dir}/%{rust_target_arch}/%{tgt_profile}/semver-formatter $RPM_BUILD_ROOT/%{_bindir}/semver-formatter

%files
%{_bindir}/semver-formatter

%changelog
* Thu Oct  6 2022 root
- initial release
