// Copyright (C) 2023 Dirk Strauss
//
// This file is part of SemVer Formatter.
//
// SemVer Formatter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SemVer Formatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use log::debug;
use semver::Version;

use crate::{FormattingErrors, OutputFormat, OutputReturnVal};

/// This method parses the given semver into RPM-compatible pieces.
///
/// Relying on information from:
/// * https://www.thegeekdiary.com/understanding-rpm-versions-and-naming-schemes/
pub fn parse_into_rpm(ver: Version) -> Result<OutputReturnVal, FormattingErrors> {
    debug!("Got this: {}", ver);
    let pre_string = ver.pre.as_str();
    let build_string = ver.build.as_str();
    let mut release_string: String = format!("{}", pre_string);
    if release_string.len() > 0 && build_string.len() > 0 {
        release_string.push_str(".");
    }
    if build_string.len() > 0 {
        release_string.push_str(build_string);
    }
    if release_string.len() == 0 {
        release_string = "1".to_string();
    }
    let ret_val = OutputReturnVal {
        major_minor_patch: format!("{}.{}.{}", ver.major, ver.minor, ver.patch),
        rpm_release: release_string,
        struct_type: OutputFormat::RPM,
        full_semver_string: ver.to_string(),
    };
    debug!("Return value for rpm is: {:?}", ret_val);
    Ok(ret_val)
}
