// Copyright (C) 2023 Dirk Strauss
//
// This file is part of SemVer Formatter.
//
// SemVer Formatter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SemVer Formatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[cfg(test)]
mod tests {
    use crate::parse_into_rpm;
    use semver::Version;

    #[test]
    fn rpm_conv_works() {
        let result = parse_into_rpm(
            Version::parse("1.2.3-alpha.23").expect("Could not generate the semver for testing!"),
        )
        .unwrap();
        assert_eq!(result.major_minor_patch, "1.2.3");
        assert_eq!(result.rpm_release, "alpha.23");
    }
    #[test]
    fn rpm_conv_works2() {
        let result = parse_into_rpm(
            Version::parse("1.2.3").expect("Could not generate the semver for testing!"),
        )
        .unwrap();
        assert_eq!(result.major_minor_patch, "1.2.3");
        assert_eq!(result.rpm_release, "1");
    }
    #[test]
    fn rpm_conv_with_build_meta() {
        let result = parse_into_rpm(
            Version::parse("1.2.3+builddate.20221013.1213")
                .expect("Could not generate the semver for testing!"),
        )
        .unwrap();
        assert_eq!(result.major_minor_patch, "1.2.3");
        assert_eq!(result.rpm_release, "builddate.20221013.1213");
    }
}
