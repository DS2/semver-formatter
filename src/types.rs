// Copyright (C) 2023 Dirk Strauss
//
// This file is part of SemVer Formatter.
//
// SemVer Formatter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SemVer Formatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#[derive(clap::ValueEnum, Clone, Debug)]
pub enum OutputFormat {
    RPM,
    DEB,
}

#[derive(Clone, Debug)]
pub struct OutputReturnVal {
    pub major_minor_patch: String,
    pub rpm_release: String,
    pub struct_type: OutputFormat,
    pub full_semver_string: String,
}

#[derive(thiserror::Error, Debug)]
pub enum FormattingErrors {
    #[error("The given semver string could not be parsed!")]
    ParseError,
}
