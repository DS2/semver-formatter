// Copyright (C) 2023 Dirk Strauss
//
// This file is part of SemVer Formatter.
//
// SemVer Formatter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SemVer Formatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::env;

use chrono::{Datelike, Timelike, Utc};
use clap::Parser;
use env_logger::Env;
use git2::Repository;
use log::debug;
use semver::Version;
use sha256::digest;

use crate::formatters::print_instructions;
use crate::parse_deb::parse_into_deb;
use crate::parse_rpm::parse_into_rpm;
use crate::types::{FormattingErrors, OutputFormat, OutputReturnVal};

pub mod formatters;
mod parse_deb;
pub mod parse_rpm;
mod tests;
pub mod types;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct MyCmdArgs {
    /// the format type to output
    #[arg(short, long, value_enum)]
    format: OutputFormat,
    /// adds a build timestamp as build information if build metadata is empty.
    #[arg(short, long, default_value_t = false)]
    add_build_timestamp: bool,
    /// flag to indicate that we also want to have a hash from the current git repo here included in our generated version.
    #[arg(short, long, default_value_t = false)]
    git_branch: bool,
    /// sets a maximum value for the git hash if enabled.
    #[arg(short, long)]
    max_hash: Option<u8>,
    /// the semver version to parse and use as target
    semver: String,
}

fn main() {
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();
    let args = MyCmdArgs::parse();
    let output_format: OutputFormat = args.format;
    let semver_input: &str = args.semver.as_str();
    let mut version = Version::parse(semver_input).expect("Not a semver version!");
    debug!("Version: {}", version);
    let build_metadata = version.clone().build;
    debug!("Build metadata is: {}", build_metadata);
    let mut build_metadata_vec: Vec<String> = Vec::new();
    if args.git_branch {
        //get current directory
        let curr_dir = env::current_dir().expect("Error when getting current directory!");
        debug!(
            "current directory should be {:?}; will check for git repo..",
            curr_dir
        );
        let curr_dir_str: &str = curr_dir
            .to_str()
            .expect("Could not parse currDir to string???");
        let repo = match Repository::open(curr_dir_str) {
            Ok(repo) => repo,
            Err(e) => panic!(
                "Could not define this directory ({}) as git repository! {}",
                curr_dir_str, e
            ),
        };
        let this_branch = repo
            .head()
            .expect("Could not find head of this repository!");
        let branch_name = this_branch.name().unwrap_or_default();
        debug!("Being on branch {}", branch_name);
        let mut sha_hash = digest(branch_name);
        sha_hash = match args.max_hash {
            None => sha_hash,
            Some(v) => {
                if v <= 0 {
                    panic!("Given max value for a hash is too small!");
                }
                sha_hash.chars().into_iter().take(v as usize).collect()
            }
        };
        build_metadata_vec.push(sha_hash);
    }
    if args.add_build_timestamp && build_metadata.as_str().len() == 0 {
        debug!("We may add our timestamp :)");
        let utc_now = Utc::now();
        let pattern = format!(
            "builddate.{:.04}{:02}{:02}.{:02}{:02}",
            utc_now.year(),
            utc_now.month(),
            utc_now.day(),
            utc_now.hour(),
            utc_now.minute()
        );
        debug!("My pattern would be {}", pattern);
        build_metadata_vec.push(pattern);
    }
    let mut formatted_version_str = format!("{}", version);
    if build_metadata_vec.len() > 0 {
        formatted_version_str = format!("{}+{}", version, build_metadata_vec.join("."));
    }
    debug!("Formatted semver is now: {}", formatted_version_str);
    version =
        Version::parse(formatted_version_str.as_str()).expect("Could not add my build timestamp..");
    let output_return_val;
    match output_format {
        OutputFormat::RPM => {
            output_return_val =
                parse_into_rpm(version).expect("Error when setting up the RPM version!");
        }
        OutputFormat::DEB => {
            output_return_val =
                parse_into_deb(version).expect("Error when setting up the RPM version!");
        }
    }
    print_instructions(output_return_val);
}
