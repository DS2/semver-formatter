// Copyright (C) 2023 Dirk Strauss
//
// This file is part of SemVer Formatter.
//
// SemVer Formatter is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// SemVer Formatter is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use crate::types::{OutputFormat, OutputReturnVal};

pub fn print_instructions(val: OutputReturnVal) {
    match val.struct_type {
        OutputFormat::RPM => {
            println!("# execute in bash:");
            println!("export FULL_SEMVER_VERSION='{}'", val.full_semver_string);
            println!("export RPM_VERSION='{}'", val.major_minor_patch);
            println!("export RPM_RELEASE='{}'", val.rpm_release);
        }
        OutputFormat::DEB => {
            println!("# execute in bash:");
            println!("export FULL_SEMVER_VERSION='{}'", val.full_semver_string);
            println!("export DEB_VERSION='{}'", val.major_minor_patch);
            println!("export DEB_REVISION='{}'", val.rpm_release);
        }
    }
}
