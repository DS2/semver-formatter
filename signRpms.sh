#!/usr/bin/env bash
# Copyright (C) 2023 Dirk Strauss
#
# This file is part of SemVer Formatter.
#
# SemVer Formatter is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SemVer Formatter is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -euo pipefail

RPM_SIGNING_KEY_FILE=${RPM_SIGNING_KEY_FILE:-}
RPM_SIGNING_KEY_PW=${RPM_SIGNING_KEY_PW:-}
GNUPGHOME=${GNUPGHOME:-/tmp/gnupghome}
GPG_TTY=${GPG_TTY:-$(tty)}
ARTIFACTS_DIR=${ARTIFACTS_DIR:-/tmp/artifacts}

install -m 0700 -d $GNUPGHOME

if [[ -z "$RPM_SIGNING_KEY_FILE" ]]; then
    echo "No signing file detected. Will ignore signing."
    exit 0
fi
if [[ ! -f "$RPM_SIGNING_KEY_FILE" ]]; then
    echo "Key file $RPM_SIGNING_KEY_FILE does not exist!"
    exit 1
fi
echo "Importing key from $RPM_SIGNING_KEY_FILE.."
cat $RPM_SIGNING_KEY_FILE | base64 -d >/tmp/myseckey.asc

echo "OK, we try to import the key now.."
if [[ -z "$RPM_SIGNING_KEY_PW" ]]; then
    echo "- with no pw"
    gpg2 --batch --yes --import /tmp/myseckey.asc
else
    echo "- with pw"
    echo $RPM_SIGNING_KEY_PW | base64 -d | gpg2 --batch --yes --passphrase-fd 0 --import /tmp/myseckey.asc
fi

# echo "Known sec keys are:"
# gpg2 --list-secret-keys --keyid-format LONG
#cat /tmp/myseckey.asc | gpg2 --with-colons --import-options show-only --import | grep fpr

echo "Finding FPR.."
FPR=$(gpg2 --list-secret-keys --with-colons | awk -F: '$1 == "fpr" {print $10; exit 0;}')
KGRIP=$(gpg2 --list-secret-keys --with-colons | awk -F: '$1 == "grp" {print $10; exit 0;}')

if [[ -z "$KGRIP" ]]; then
    echo "Could not find grip for key $FPR!"
    exit 1
fi
/usr/libexec/gpg-preset-passphrase --passphrase "$(echo $RPM_SIGNING_KEY_PW | base64 -d)" --preset $KGRIP

if [[ -z "$FPR" ]]; then
    echo "No FPR found??"
    exit 1
fi

echo "FPR is $FPR"
if [[ ! -f "$HOME/.rpmmacros" ]]; then
    echo "Creating RPM Macros config.."
    echo "%_gpg_name $FPR" >$HOME/.rpmmacros
    echo "%_gpg_path $GNUPGHOME" >>$HOME/.rpmmacros
# else
#     echo "RPM Macros exist already:"
#     cat $HOME/.rpmmacros
fi
if [[ -d $ARTIFACTS_DIR ]]; then
    echo "Now, rpm sign is called"
    rpmsign --addsign $ARTIFACTS_DIR/*.rpm
else
    echo "No artifacts dir found. Ignoring signing.."
fi
